//
//  Photos.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import MapKit

class PhotoItem: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D 
    var photoTitle: String
    var username: String
    var imageUrl: URL
    var longitude: String
    var latitude: String
    var id: String
    
    init(username: String,photoTitle: String, imageUrl: URL, longitude: String, latitude: String, id: String, coordinate:CLLocationCoordinate2D) {

        self.photoTitle = photoTitle
        self.imageUrl = imageUrl
        self.longitude = longitude
        self.latitude = latitude
        self.id = id
        self.username = username
        self.coordinate = coordinate
        if let lat = Double(latitude),let lon = Double(longitude) {
            self.coordinate = CLLocationCoordinate2D(latitude: lat , longitude: lon)
            
        }
        
    }
    convenience init(username: String,photoTitle: String, imageUrl: URL, longitude: String, latitude: String, id: String) {
        if let lat = Double(latitude),let lon = Double(longitude) {
            let coordinate = CLLocationCoordinate2D(latitude: lat , longitude: lon)
            self.init(username: username, photoTitle: photoTitle, imageUrl: imageUrl, longitude: longitude, latitude: latitude, id: id, coordinate: coordinate)
            return
        }
        let coordinate = CLLocationCoordinate2D(latitude: 0 , longitude: 0)
        self.init(username: username,photoTitle: photoTitle, imageUrl: imageUrl, longitude: longitude, latitude: latitude, id: id, coordinate: coordinate)
    }
    
    
    
}
