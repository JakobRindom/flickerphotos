//
//  DataAnswerToKeyHolder.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class DataAnswerToKeyHolder: NSObject {
    
    var data: Data?
    var requestStatus: RequestStatus
    var key: String
    
    init(data: Data?, status: RequestStatus, key: String) {
        self.data = data
        self.requestStatus = status
        self.key = key
    }
}
