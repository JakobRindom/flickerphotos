//
//  User.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 30/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

class User : NSObject{
    
    static private var singleton : User?
    //UserDefaults for saving token
    static var userDefaults = UserDefaults()
    //Key for accessing/saving token
    static var tokenKey = "accessToken"
    
    var userName : String?
    var name : String?
    var userId : String?
    var accessToken : URL?{
        get{
            let token = User.userDefaults.url(forKey: User.tokenKey)
            return token
        }
    }
    
    //init
    class var shared: User {
        struct Singleton {
            static let instance = User()
        }
        return Singleton.instance
    }
    
 
    //deinit
    class func destroy() {
        self.userDefaults.removeObject(forKey: self.tokenKey)
        singleton = nil
    }
    
    //Sets User data to signleton object
    func setProfileInfo(userName : String?,fullName: String?,userId: String?){
        self.userName = userName
        self.name = fullName
        self.userId = userId
        
    }
    //Saving access token in user defaults
    func saveAccessToken(url: URL){
        let token = User.userDefaults.value(forKey: User.tokenKey) as? URL
        if token == nil{
            User.userDefaults.set(url, forKey: User.tokenKey)
        }
        
        
    }

}
