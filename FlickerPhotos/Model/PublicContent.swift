//
//  PublicContent.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

// To parse the JSON, add this file to your project and do:
//
//   let flickr = try Flickr(json)


class Flickr: Codable {
    let title, link, description, modified: String
    let generator: String
    let items: [Item]
    
    init(title: String, link: String, description: String, modified: String, generator: String, items: [Item]) {
        self.title = title
        self.link = link
        self.description = description
        self.modified = modified
        self.generator = generator
        self.items = items
    }
}

class Item: Codable {
    let title, link: String
    let media: Media
    let dateTaken, description, published, author: String
    let authorID, tags: String
    
    enum CodingKeys: String, CodingKey {
        case title, link, media
        case dateTaken = "date_taken"
        case description, published, author
        case authorID = "author_id"
        case tags
    }
    
    init(title: String, link: String, media: Media, dateTaken: String, description: String, published: String, author: String, authorID: String, tags: String) {
        self.title = title
        self.link = link
        self.media = media
        self.dateTaken = dateTaken
        self.description = description
        self.published = published
        self.author = author
        self.authorID = authorID
        self.tags = tags
    }
}

class Media: Codable {
    let m: String
    
    init(m: String) {
        self.m = m
    }
}

// MARK: Convenience initializers

extension Flickr {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(Flickr.self, from: data)
        self.init(title: me.title, link: me.link, description: me.description, modified: me.modified, generator: me.generator, items: me.items)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Item {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(Item.self, from: data)
        self.init(title: me.title, link: me.link, media: me.media, dateTaken: me.dateTaken, description: me.description, published: me.published, author: me.author, authorID: me.authorID, tags: me.tags)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Media {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(Media.self, from: data)
        self.init(m: me.m)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
