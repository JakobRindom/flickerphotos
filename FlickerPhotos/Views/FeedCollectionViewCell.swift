//
//  FeedCollectionViewCell.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 28/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class FeedCollectionViewCell: UICollectionViewCell, ImageFetcherProtocol {

    @IBOutlet weak var imgView: UIImageView!
    private var imageFetcher: ImageFetcher?
    private var imageCacheManager: ImageCacheManager?
    
    
    var photoItem:PhotoItem? {
        didSet {
            if let photodata = photoItem {
                self.getImage(photodata.imageUrl.absoluteString)
            }
        }
    }

    var cellData: Item? {
        didSet {
            if let photodata = cellData {
                self.getImage(photodata.media.m)
            }
        }
    }
    
    func isRightImage(_ imageFileUrl: String) -> Bool {
        if let celldata = self.cellData {
            if celldata.media.m + "FeedCollectionViewCell" == imageFileUrl {
                return true
            }
        }
        if let photoItem = self.photoItem {
            if photoItem.imageUrl.absoluteString + "FeedCollectionViewCell" == imageFileUrl {
                return true
            }
        }
        return false
    }
    
    
    func getImage(_ imageURl: String)  {
        if self.imgView.image == nil {
           // let url = Helper.imageURL(imageURl, size: self.imgAlbumCover.bounds.size, widthFromAPI: widthFromAPI, heightFromAPI: heightFromAPI)
            self.imageCacheManager = ImageCacheManager()
            
            if let imgChacheMan = self.imageCacheManager {
                if let image = imgChacheMan.cachedImage(url: imageURl + "FeedCollectionViewCell") {
                    // Cached: set immediately.
                    self.imgView.image = image
                    self.imgView.alpha = 1
                } else {
                    // Not cached, so load then fade it in.
                    self.imgView.alpha = 0
                    
                    self.imageFetcher = ImageFetcher(delegate: self, imageUrl: imageURl, type: "FeedCollectionViewCell")
                    
                    //      self.coverImage.downloadedFrom(link: song.coverUrl , view: self.coverImage)
                    
                }
            }
        }
    }
    
    func imageReady(imageFileUrl: String, image: UIImage) {
        if self.isRightImage(imageFileUrl) {
            self.imgView?.image = image
            UIView.animate(withDuration: 0.3) {
                self.imgView.alpha = 1
            }
        }
    }
    
    func imageFailed(errorCode: Int) {
        print("errorcode: \(errorCode)")
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        self.cellData = nil
        self.imgView.image = nil
        self.imageFetcher = nil
        self.imageCacheManager = nil
        self.photoItem = nil
    }

}
