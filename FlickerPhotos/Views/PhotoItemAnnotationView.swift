//
//  PhotosAnnotationView.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 02/05/2018.
//  Copyright © 2018 me. All rights reserved.
//

import MapKit

final class PhotoItemAnnotationView:  MKAnnotationView,ImageFetcherProtocol {
    
    private var imageFetcher: ImageFetcher?
    private var imageCacheManager: ImageCacheManager?
    override var annotation: MKAnnotation? {
        didSet {
            if let photoItem = annotation as? PhotoItem {
            
            clusteringIdentifier = "PhotoItem"
                self.getImage(photoItem.imageUrl.absoluteString)
            }
        }
    }
    
    func isRightImage(_ imageFileUrl: String) -> Bool {
        if let photoItem = annotation as? PhotoItem {
            if photoItem.imageUrl.absoluteString + "PhotoItemAnnotationView" == imageFileUrl {
                return true
            }
        }
        return false
    }
    
    
    func getImage(_ imageURl: String)  {
        if self.image == nil {
            self.imageCacheManager = ImageCacheManager()
            if let imgChacheMan = self.imageCacheManager {
                if let image = imgChacheMan.cachedImage(url: imageURl + "PhotoItemAnnotationView") {
                   
                    // Cached: set immediately.
                    self.image = image
                   
                } else {
                    // Not cached, then load.
                    self.imageFetcher = ImageFetcher(delegate: self, imageUrl: imageURl, type: "PhotoItemAnnotationView")
                 
                }
            }
        }
    }
    
    func imageReady(imageFileUrl: String, image: UIImage) {
        if self.isRightImage(imageFileUrl) {
            let size = CGSize(width: 50, height: 50)
            UIGraphicsBeginImageContext(size)
            image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            self.image = resizedImage
        }
    }
    
    func imageFailed(errorCode: Int) {
        print("errorcode: \(errorCode)")
    }
    
    override func prepareForReuse() {
        self.imageFetcher = nil
        self.imageCacheManager = nil
        self.annotation = nil
        
    }
    
}
