//
//  LoginViewController.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 30/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import FlickrKit
import WebKit

class LoginViewController: UIViewController, WKUIDelegate, WKNavigationDelegate  {

    @IBOutlet weak var webView: WKWebView!
    var flickrHelper: FlickrHelper?
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = webView.url {
            if let scheme = url.scheme {
                if scheme == "flikrphotos" {
                    User.shared.saveAccessToken(url: url)
                    self.flickrHelper = FlickrHelper()
                    self.flickrHelper?.checkAuthentication(callBackURL: url, sender: self, { () -> Void? in
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                }
            }  else if url.absoluteString == "https://m.flickr.com/#/home" {
                //else navigate to login
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 , execute: {
                    _ = self.dismiss(animated: true, completion: nil)
                })
            }
         decisionHandler(.allow)
        }
    }

    private func auth(){
        let callbackURLString = "FlikrPhotos://auth"
        let url = URL(string: callbackURLString)
        FlickrKit.shared().beginAuth(withCallbackURL: url!, permission: FKPermission.delete, completion: { (url, error) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                
                if ((error == nil)) {
                    let urlRequest = NSMutableURLRequest(url: url!, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 30)
                    self.webView.load(urlRequest as URLRequest)
                } else {
                    
                    if let error = error as NSError? {
                            AlertHelper.showAlert(sender: self, title: "Error", message: error.localizedDescription)
                        return
                    }
                    
                    
                }
            });
        })
    }
    
    //MARK: Life
    
    override func loadView() {
        super.loadView()
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.auth()
    }


}
