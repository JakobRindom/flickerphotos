//
//  MapViewController.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 25/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FlickrKit


class MapViewController: UIViewController, CLLocationManagerDelegate,MKMapViewDelegate, APIMapContentDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var dictPhotoItems: [String:Any] = [:]
    var flickrHelper  = FlickrHelper()
    var boolIsLoggedIn:Bool = false
    var savedLocation:CLLocationCoordinate2D?
    var selectedItem: PhotoItem?
    var startLocation: CLLocation?
    let locationManager = CLLocationManager()
    
    
    //MARK: Delegates - APIMapContentDelegate
    func didRecieveMapContent(_ photoItems: [PhotoItem]) {
        for item in photoItems {
            DispatchQueue.main.async {
       //Making sure the same images is not added to the map more than once.
        if self.dictPhotoItems[item.id] == nil {
            self.dictPhotoItems[item.id] = item
            self.mapView.addAnnotation(item)
            
            }
        }
    }
    }
    
    func mapContentFailed(status: RequestStatus) {
    }
   
    //MARK: - Mapview
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = mapView.centerCoordinate
        if self.boolIsLoggedIn {
        let api = APIMapContent(delegate: self)
        api.getImagesForLocation(Float(center.latitude), longitude:  Float(center.longitude))
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view is PhotoItemAnnotationView {
            if let photoItem = view.annotation as? PhotoItem {
                self.selectedItem = photoItem
                self.performSegue(withIdentifier: "ShowDetailsSegue", sender: self)
            }
        } else if view is ClusterAnnotationView {
            let currentSpan = mapView.region.span
            let zoomSpan = MKCoordinateSpan(latitudeDelta: currentSpan.latitudeDelta / 2.0, longitudeDelta: currentSpan.longitudeDelta / 2.0)
            let zoomCoordinate = view.annotation?.coordinate ?? mapView.region.center
            let zoomed = MKCoordinateRegion(center: zoomCoordinate, span: zoomSpan)
            mapView.setRegion(zoomed, animated: true)
        }
    }

    
    //MARK: - Life
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if User.shared.accessToken != nil{
            self.flickrHelper.login(sender: self, { (error) -> Void in
                if error == nil {
                    self.boolIsLoggedIn = true
                        if let location = self.startLocation {
                            let api = APIMapContent(delegate: self)
                            api.getImagesForLocation(Float(location.coordinate.latitude), longitude:  Float(location.coordinate.longitude))
                    }
                }
                print(error)
            })
        } else {
            //Else go to login screen.
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("mapview_title", comment: "Map")
    
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.register(PhotoItemAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        mapView.register(ClusterAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = true
        
        //Zoom to user location
        if let location = self.locationManager.location {
            print(location)
            self.startLocation = location
            let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 5000, 5000)
            print(region)
               mapView.setRegion(region, animated: true)
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetailsSegue" {
            let controller = segue.destination as! ImageViewController
            if let item = self.selectedItem {
                controller.selectedPhotoItem = item
            }
        }
    }
}
