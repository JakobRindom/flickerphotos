//
//  MyContentCollectionViewController.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import FlickrKit

class MyContentViewCollectionController: UICollectionViewController, APIMyContentDelegate, UICollectionViewDelegateFlowLayout {
    
    
    
    var arrItems: [PhotoItem] = []
    var BoolWaitingForMoreImages: Bool = false
    var intCurrentPage:Int = 0
    let intNumberPages: Int = 20
    var selectedItem: PhotoItem?
    var flickrHelper = FlickrHelper()
    
    // MARK: - Delegate - APIMyContentDelegate
    
    func didRecieveMyContent(_ photoItems: [PhotoItem]) {
        self.arrItems.append(contentsOf: photoItems)
        DispatchQueue.main.async {
            var arrIndexPath: [IndexPath] = []
            
            for number in self.arrItems.count-photoItems.count..<self.arrItems.count {
                arrIndexPath.append(IndexPath(item: number, section: 0))
            }
            self.collectionView?.insertItems(at: arrIndexPath)
            
            //Only try to load more images, if there are more to get.
            if photoItems.count == self.intNumberPages {
                self.BoolWaitingForMoreImages = false
            }
        }
    }

    
    func myContentFailed(status: RequestStatus) {
        print(status)
    }
    
    // MARK: UICollectionView
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.arrItems[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
        cell.photoItem = item
        
        
        //Load more items
        if !self.BoolWaitingForMoreImages && self.arrItems.count-3 == indexPath.item {
            self.BoolWaitingForMoreImages = true
            let api = APIMyContent(delegate: self)
            self.intCurrentPage = self.intCurrentPage+1
            api.getMyContent(self.intCurrentPage, perpage: intNumberPages)
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width:  self.view.bounds.size.width/3 - 8, height:  self.view.bounds.size.width/3 - 8 )
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedItem = self.arrItems[indexPath.item]
        performSegue(withIdentifier: "ShowDetailsSegue", sender: self)
        
        
    }
    
    
    // MARK: - Life
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //If Contains the Access Token, Get users images
        if User.shared.accessToken != nil{
            self.flickrHelper.login(sender: self, { (error) -> Void in
                if self.arrItems.count == 0 {
                    //Call API
                    let api = APIMyContent(delegate: self)
                    api.getMyContent(0, perpage: self.intNumberPages)
                }
            })
        } else {
            //Else go to login screen.
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("mycontent_my_images", comment: "My images")
        
        // Register cell classes
        self.collectionView?.register(UINib(nibName: "FeedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FeedCollectionViewCell")
        
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetailsSegue" {
            let controller = segue.destination as! ImageViewController
            if let item = self.selectedItem {
                controller.selectedPhotoItem = item
            }
        }
    }
    
}
