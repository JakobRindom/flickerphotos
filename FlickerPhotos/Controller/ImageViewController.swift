//
//  ImageViewController.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 25/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, ImageFetcherProtocol, UIScrollViewDelegate {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var scrollViewImage: UIScrollView!
    private var imageFetcher: ImageFetcher?
    private var imageCacheManager: ImageCacheManager?

    var selectedItem: Item?
    var selectedPhotoItem: PhotoItem?

    @objc func dismissView()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Saving Image here
    @IBAction func save(_ sender: AnyObject) {
        UIImageWriteToSavedPhotosAlbum(imgView.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: NSLocalizedString("Error", comment: "Error") , message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
           
            let ac = UIAlertController(title: "", message: NSLocalizedString("imageview_saved", comment: "The image is now saved"), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }

    func getImage(_ imageURl: String)  {
        if self.imgView.image == nil {
            self.imageCacheManager = ImageCacheManager()
            var bigImageUrl = imageURl.replacingOccurrences(of: "m.jpg", with: "b.jpg")
            bigImageUrl = bigImageUrl.replacingOccurrences(of: "s.jpg", with: "b.jpg")
            bigImageUrl = bigImageUrl.replacingOccurrences(of: "n.jpg", with: "b.jpg")
            if let imgChacheMan = self.imageCacheManager {
                if let image = imgChacheMan.cachedImage(url: bigImageUrl + "ImageViewController") {
                    // Cached: set immediately.
                    self.imgView.image = image
                    self.imgView.alpha = 1
                } else {
                    // Not cached, so load then fade it in.
                    self.imgView.alpha = 0
                    self.imageFetcher = ImageFetcher(delegate: self, imageUrl: bigImageUrl, type: "ImageViewController")
                }
            }
        }
    }
    
    func imageReady(imageFileUrl: String, image: UIImage) {
        self.spinner.stopAnimating()
        if let selItem = self.selectedItem {
            let bigImageUrl = selItem.media.m.replacingOccurrences(of: "m.jpg", with: "b.jpg")
            if bigImageUrl + "ImageViewController" == imageFileUrl {
                self.imgView?.image = image
                UIView.animate(withDuration: 0.3) {
                    self.imgView.alpha = 1
                }
            }
        } else if let photoItem = self.selectedPhotoItem {
            var bigImageUrl = photoItem.imageUrl.absoluteString.replacingOccurrences(of: "n.jpg", with: "b.jpg")
            bigImageUrl = bigImageUrl.replacingOccurrences(of: "s.jpg", with: "b.jpg")
            if bigImageUrl + "ImageViewController" == imageFileUrl {
                self.imgView?.image = image
                UIView.animate(withDuration: 0.3) {
                    self.imgView.alpha = 1
                }
            }
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView
    }
    
    
    func imageFailed(errorCode: Int) {
        print("errorcode: \(errorCode)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.spinner.startAnimating()
        self.spinner.isHidden = false
        self.spinner.hidesWhenStopped = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.scrollViewImage.delegate = self
        if let selItem = self.selectedItem {
            self.getImage(selItem.media.m)
            self.lblTitle.text = selItem.title
            self.lblAuthor.text = selItem.author

        } else if let selPhotoItem = self.selectedPhotoItem {
            self.getImage(selPhotoItem.imageUrl.absoluteString)
            self.lblTitle.text = selPhotoItem.photoTitle
            self.lblAuthor.text = selPhotoItem.username
        }
        
        let gestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.imgView.image = nil
        self.selectedPhotoItem = nil
        self.selectedItem = nil
        self.imageFetcher = nil
        self.imageCacheManager = nil
    }
}
