//
//  UserProfileViewController.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 25/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import FlickrKit

class UserProfileViewController: UIViewController {
    
    var flickrHelper = FlickrHelper()
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var btnLogOut: UIButton!
    
    
    @IBAction func btnLogOut(_ sender: Any) {
        //Log out
        if(FlickrKit.shared().isAuthorized) {
            FlickrKit.shared().logout()
            User.destroy()
            _ = self.navigationController?.popToRootViewController(animated: true)
            self.lblUserName.text = NSLocalizedString("userprofile_logged_out", comment: "You have succesfully logged out")
            self.btnLogOut.setTitle(NSLocalizedString("userprofile_log_in_btn", comment: "Log in"), for: UIControlState())
            self.lblFullName.text = ""
        } else {
             //Log in -> navigate to LoginViewController
           self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }
    }
    
    //MARK: Life
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //If Contains the Access Token, Get users images
        self.spinner.startAnimating()
        self.spinner.isHidden = false
        self.spinner.hidesWhenStopped = true
        if User.shared.accessToken != nil{
            self.flickrHelper.login(sender: self, { (error) -> Void in
                self.spinner.stopAnimating()
                if error == nil {
                    self.lblFullName.text = User.shared.name
                    if let username = User.shared.userName {
                        self.lblUserName.text = "\(NSLocalizedString("userprofile_username", comment: "Username")): \(username)"
                        self.btnLogOut.setTitle(NSLocalizedString("userprofile_log_out_btn", comment: "Log out"), for: UIControlState())

                    }
                }
            })
        } else {
             self.spinner.stopAnimating()
            
            //Else go to login screen.
            self.btnLogOut.setTitle(NSLocalizedString("userprofile_log_in_btn", comment: "Log in"), for: UIControlState())
            self.lblUserName.text = NSLocalizedString("userprofile_log_in_to_see_info", comment: "Log in to see info")
            self.lblFullName.text = ""

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("userprofile_title", comment: "User Profile")
        self.btnLogOut.setTitle("", for: UIControlState()) 
        self.lblUserName.text = ""
        self.lblFullName.text = ""
        
    }
  
}
