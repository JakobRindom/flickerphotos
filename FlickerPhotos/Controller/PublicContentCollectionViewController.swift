//
//  PublicContentCollectionViewController.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit


class PublicContentCollectionViewController: UICollectionViewController, APIPublicContentDelegate, UICollectionViewDelegateFlowLayout {
    
    var items: [Item] = []
    var waitingForMoreImages: Bool = false
    var selectedItem: Item?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PublicContentCollectionViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        let api = APIPublicContent(delegate: self)
        api.getPublicImages()
        
    }
    
     // MARK: - Delegate - APIPublicContentDelegate
    func didRecievePublicContent(data: Data) {
        refreshControl.endRefreshing()
        do {
            var newPhotos: [Item] = []
        let photos = try Flickr(data: data)
            for photo in photos.items {
                print(photo.link)
            if !self.items.contains(where: { $0.link == photo.link }) {
                newPhotos.append(photo)
                }
            }
            
            self.items.insert(contentsOf: newPhotos, at: 0)
            DispatchQueue.main.async {
                var arrIndexPath: [IndexPath] = []
                for number in 0..<newPhotos.count {
                      
                    arrIndexPath.append(IndexPath(item: number, section: 0))
                }

                self.collectionView?.insertItems(at: arrIndexPath)
                self.waitingForMoreImages = false
            }
        } catch {
        }
    }
    
    func publicContentFailed(status: RequestStatus) {
        print("Failed:status")
    }

    // MARK: UICollectionView

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.items[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
        cell.cellData = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:  self.view.bounds.size.width/3 - 8, height:  self.view.bounds.size.width/3 - 8 )
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         self.selectedItem = self.items[indexPath.item]
       performSegue(withIdentifier: "ShowDetailsSegue", sender: self)
    }

    
     // MARK: - Life
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let api = APIPublicContent(delegate: self)
        api.getPublicImages()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.register(UINib(nibName: "FeedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FeedCollectionViewCell")
         self.title = NSLocalizedString("publiccontent_title", comment: "Public Images")
        self.collectionView?.addSubview(self.refreshControl)
    }

     // MARK: - Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetailsSegue" {
            let controller = segue.destination as! ImageViewController
            controller.selectedItem = self.selectedItem
        }
     }
}
