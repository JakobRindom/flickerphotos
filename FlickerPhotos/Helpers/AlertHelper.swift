//
//  Alart.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 02/05/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class AlertHelper: NSObject {
    

static func showAlert(sender: UIViewController, title: String, message : String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(okButton)
    sender.present(alert, animated: true, completion: nil)
    }
}
