//
//  ImageCacheManager.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 28/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class ImageCacheManager {
    
    static let imageCache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        
        cache.name = "ImageCacheManager"
        // Max 120 images in memory.
        cache.countLimit = 120
        // Max MB used.
        cache.totalCostLimit = 120 * 1024 * 1024
        
        return cache
    }()
    
    // MARK: Image Caching Methods
    
    func cachedImage(url: String) -> UIImage? {
        return ImageCacheManager.imageCache.object(forKey: url as NSString)
    }
    
    func fetchImage(url: String, completion: @escaping ((UIImage?) -> Void)) {
        let realUrl = URL(string: url)
        let task = URLSession.shared.dataTask(with: realUrl!) { (data, response, error) in
            guard error == nil, let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200, let data = data else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            if let image = UIImage(data: data) {
                ImageCacheManager.imageCache.setObject(image, forKey: url as NSString)
                DispatchQueue.main.async {
                    completion(image)
                }
            } else {
                DispatchQueue.main.async {
                    completion(UIImage())
                }
            }
        }
        
        task.resume()
    }
    
}

