//
//  ImageFetcher.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 28/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

protocol ImageFetcherProtocol: class {
    func imageReady(imageFileUrl: String, image: UIImage)
    func imageFailed(errorCode: Int)
}

class ImageFetcher: NSObject, APIDataProtocol {
   
    private weak var delegate: ImageFetcherProtocol?
    private var imageUrl: String?
    private var type: String?
    
    init(delegate: ImageFetcherProtocol, imageUrl: String, type: String) {
        self.delegate = delegate
        self.imageUrl = imageUrl
        self.type = type

        super.init()
        
        self.beginImageRequest(imageUrl: imageUrl)
    }
    
    
    private func beginImageRequest(imageUrl: String){
        
        let api = API(delegate: self)
        api.getDataFromAPI(APIUrlWithParameters: imageUrl, key:"imageFetcher" )
    }
    
    //MARK: - Callbacks
    
    private func callbackFailed(errorCode: Int){
        delegate?.imageFailed(errorCode: errorCode)
    }
    
    private func callbackSucceded(imageUrl: String, image: UIImage){
        if let portraitId = self.imageUrl, let extraKey = self.type {
            
            self.delegate?.imageReady(imageFileUrl: portraitId+extraKey, image: image)
        }else{
            self.callbackFailed(errorCode: 123456)
        }
    }
    
    //MARK: - Delegate

    func didReceiveAPIData(dataAnswerToKeyHolder: DataAnswerToKeyHolder) {
            if let imgData = dataAnswerToKeyHolder.data {
                let img = UIImage(data: imgData)
                if let img = img, let portraitID = self.imageUrl,let extraKey = self.type {
                    let key:NSString = portraitID + extraKey as NSString
                    ImageCacheManager.imageCache.setObject(img, forKey: key)
                  
                    self.callbackSucceded(imageUrl: portraitID, image: img)
               
                }else{
                    callbackFailed(errorCode: 123456)
                }
            }
            }
}
