//
//  PublicContentManager.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

protocol APIPublicContentDelegate: class {
    func didRecievePublicContent(data: Data)
    func publicContentFailed(status: RequestStatus)
}

final class APIPublicContent: NSObject, APIDataProtocol {
    
    private weak var delegate: APIPublicContentDelegate?
    
    init(delegate: APIPublicContentDelegate) {
        self.delegate = delegate
    }
    
    //Get images from feed, Paging is not a thing in a feed, and some images will show up many times.
    func getPublicImages()  {
          let queryString = "https://api.flickr.com/services/feeds/photos_public.gne?per_page=20&format=json&nojsoncallback=1"

        let api = API(delegate: self)
        api.getDataFromAPI(APIUrlWithParameters: queryString, key: "getPublicImages")
    }

    //MARK: - Delegates
    
    
    //Handle request data, start parsing and make callback.
    func didReceiveAPIData(dataAnswerToKeyHolder: DataAnswerToKeyHolder) {
        if dataAnswerToKeyHolder.requestStatus == .success{
            if let data = dataAnswerToKeyHolder.data {
                 delegate?.didRecievePublicContent(data: data)
            }else{
                self.delegate?.publicContentFailed(status: .devError )
            }
        }else{
            self.delegate?.publicContentFailed(status: dataAnswerToKeyHolder.requestStatus)
        }
    }

}
