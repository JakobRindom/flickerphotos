//
//  PublicContentManager.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import FlickrKit

protocol  APIMyContentDelegate: class {
    func didRecieveMyContent(_ photoItems: [PhotoItem])
    func myContentFailed(status: RequestStatus)
}

final class APIMyContent: NSObject {
    
    private weak var delegate: APIMyContentDelegate?
    
    init(delegate: APIMyContentDelegate) {
        self.delegate = delegate
    }
  
    //Get users content.
    func getMyContent(_ page: Int, perpage: Int)  {
           var photoItems = [PhotoItem]()
            if FlickrKit.shared().isAuthorized {
                guard let userId = User.shared.userId else { return  }

                FlickrKit.shared().call("flickr.photos.search", args: ["user_id":userId,"per_page":"\(perpage)","page":"\(page)","extras":"owner_name"], completion: { (response, error) -> Void in

                    if error == nil{
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let response = response, let photoArray =
                                FlickrKit.shared().photoArray(fromResponse: response) {

                                for photoDictionary in photoArray {
                                    let photoURL = FlickrKit.shared().photoURL(for: FKPhotoSize.small320, fromPhotoDictionary: photoDictionary)
                                    
                                  
                                        if let photoId = photoDictionary["id"] as? String,let title = photoDictionary["title"] as? String, let username = photoDictionary["ownername"] as? String {
                                            let photoItem: PhotoItem = PhotoItem(username:username, photoTitle: title, imageUrl: photoURL, longitude: "", latitude: "", id: photoId)
                                            photoItems.append(photoItem)
                                        }
                                    }
                                
                                self.delegate?.didRecieveMyContent(photoItems)

                            }
                        })
                    }  else{
                        if let error = error as NSError?, let vc = self.delegate as? UIViewController {
                            AlertHelper.showAlert(sender: vc , title: "Error", message: error.localizedDescription)
                            return
                        }
                      
                    }
                })
 }
        }
 
    
}
