//
//  API.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

protocol APIDataProtocol: class {
    func didReceiveAPIData(dataAnswerToKeyHolder: DataAnswerToKeyHolder)
}

enum RequestStatus {
    case success  //200
    case notFound //404
    case serverError //500
    case devError //123456 fejl som udvikler kaster videre
    case noInternet //ingen internet (-1)
    case unknown //-999
    case parseDataError //Når data fra request ikke kan parses som forventet
}

class API: NSObject, URLSessionDelegate {

    var delegate: APIDataProtocol?
    var task: URLSessionDataTask?
    
    init(delegate: APIDataProtocol?) {
        self.delegate = delegate
    }
    

func getDataFromRequest(request: NSMutableURLRequest, key: String) {
    
    let config = URLSessionConfiguration.default
    let queue = OperationQueue.main
    let session = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: queue)
    self.task = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
        
        if(error != nil)
        {
            if(error!._code == -999)
            {
                let dataAnswerToKeyHolder = DataAnswerToKeyHolder(data: nil, status: .unknown , key: key)
                self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: dataAnswerToKeyHolder)
                return;
            }
            let dataAnswerToKeyHolder = DataAnswerToKeyHolder(data: nil, status: .noInternet , key: key)
            self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: dataAnswerToKeyHolder)
            return;
        }
        
        guard let httpResponse = response as? HTTPURLResponse
            else {
                self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: DataAnswerToKeyHolder(data: nil, status: .unknown, key: key))
                return
        }
        
        if httpResponse.statusCode == 500{
            let dataAnswerToKeyHolder = DataAnswerToKeyHolder(data: nil, status: .serverError , key: key)
            self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: dataAnswerToKeyHolder)
        }else if httpResponse.statusCode == 404{
            let dataAnswerToKeyHolder = DataAnswerToKeyHolder(data: nil, status: .notFound , key: key)
            self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: dataAnswerToKeyHolder)
        }else if httpResponse.statusCode == 200{
            let dataAnswerToKeyHolder = DataAnswerToKeyHolder(data: data!, status: .success , key: key)
            self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: dataAnswerToKeyHolder)
        }else{
            let dataAnswerToKeyHolder = DataAnswerToKeyHolder(data: data!, status: .unknown, key: key)
            self.delegate?.didReceiveAPIData(dataAnswerToKeyHolder: dataAnswerToKeyHolder)
        }
    }
    self.task!.resume()
}

func getDataFromAPI(APIUrlWithParameters: String, key: String)
{
    let pathString = APIUrlWithParameters.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    
    if let pathString = pathString {
        let pathUrl = URL(string:pathString)
        
        if let pathUrl = pathUrl {
            let request = NSMutableURLRequest(url: pathUrl)
            request.httpMethod = "GET"
            self.getDataFromRequest(request: request, key: key)
        }
        
    }else{
        //FIXME: Errorhandling
    }
}

    
}

