//
//  PublicContentManager.swift
//  FlickrPhotos
//
//  Created by Jakob Ny on 26/04/2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import FlickrKit

protocol  APIMapContentDelegate: class {
    func didRecieveMapContent(_ photoItems: [PhotoItem])
    func mapContentFailed(status: RequestStatus)
}

final class APIMapContent: NSObject  {
    
    private weak var delegate: APIMapContentDelegate?
    
    init(delegate: APIMapContentDelegate) {
        self.delegate = delegate
    }
    
    //Getting the images for the specified location
    func getImagesForLocation(_ latitude: Float, longitude: Float)  {
        var photoItems = [PhotoItem]()
        if FlickrKit.shared().isAuthorized {
            FlickrKit.shared().call("flickr.photos.search", args: ["lat":"\(latitude)","lon":"\(longitude)","per_page":"40","extras":"geo,owner_name"], completion: { (response, error) -> Void in
                if error == nil{
                    DispatchQueue.main.async(execute: { () -> Void in
                        if let response = response, let photoArray = FlickrKit.shared().photoArray(fromResponse: response) {
                            for photoDictionary in photoArray {
                                let photoURL = FlickrKit.shared().photoURL(for: FKPhotoSize.smallSquare75, fromPhotoDictionary: photoDictionary)
                                if let photoId = photoDictionary["id"] as? String,let title = photoDictionary["title"] as? String, let longitude = photoDictionary["longitude"] as? String, let latitude = photoDictionary["latitude"] as? String,
                                   let username = photoDictionary["ownername"] as? String
                                    {
                                    let photoItem: PhotoItem = PhotoItem(username: username,photoTitle: title, imageUrl: photoURL, longitude: longitude, latitude: latitude, id: photoId)
                                    photoItems.append(photoItem)
                                }
                            }
                            self.delegate?.didRecieveMapContent(photoItems)
                        }
                    })
                }
                else{
                    if let error = error as NSError?,    let vc = self.delegate as? UIViewController {
                        AlertHelper.showAlert(sender: vc, title: "Error", message: error.localizedDescription)
                        return
                    }
                }
            })
        }
    }
}
